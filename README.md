```ruby
{
'en_US.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/en/en_US.dic',
'en_US.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/en/en_US.aff',
'en_GB.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/en/en_GB.dic',
'en_GB.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/en/en_GB.aff',
'es_ANY.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/es/es_ANY.dic',
'es_ANY.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/es/es_ANY.aff',
'de_DE_frami.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/de/de_DE_frami.dic',
'de_DE_frami.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/de/de_DE_frami.aff',
'it_IT.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/it_IT/it_IT.dic',
'it_IT.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/it_IT/it_IT.aff',
'fr.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/fr_FR/fr.dic',
'fr.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/fr_FR/fr.aff',
'nl_NL.dic' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/nl_NL/nl_NL.dic',
'nl_NL.aff' => 'https://raw.githubusercontent.com/LibreOffice/dictionaries/master/nl_NL/nl_NL.aff',
'la_LA.dic' => 'https://raw.githubusercontent.com/wooorm/dictionaries/main/dictionaries/la/index.dic',
'la_LA.aff' => 'https://raw.githubusercontent.com/wooorm/dictionaries/main/dictionaries/la/index.aff',
'pt_PT.dic' => 'https://raw.githubusercontent.com/wooorm/dictionaries/main/dictionaries/pt-PT/index.dic',
'pt_PT.aff' => 'https://raw.githubusercontent.com/wooorm/dictionaries/main/dictionaries/pt-PT/index.aff'
}
```


### convert dict to txt

`unmunch en_US.dic en_US.aff > en_US1.txt`


### LT resources

https://github.com/languagetool-org/languagetool/tree/master/languagetool-language-modules/en/src/main/resources/org/languagetool/resource/en
